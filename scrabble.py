import re
import random
import time


def get_words(starts_with, length):
    # reads lines from text file and creates a list of all words
    # create a list of words that we can replace with his current words.

    text_file = open('word_list.txt')
    file_text = text_file.read()
    text_file.close()
    word_list = file_text.split()
    words = [w for w in word_list if w.startswith(starts_with) and len(w) == length]
    return words


def scrabble():
    # Calls for an infinite loop that keeps executing
    # until an exception occurs or application is forcefully quit CTRL+D or whatever
    print("\033[1;32;40m Welcome to your Scrabble Assistant!  \n")
    print("\033[1;32;40m Please type some words seperated by a space, without numbers or special characters. \n")
    while True:
        try:
            sentence = input("\033[1;37;40m").lower()

            # If an illegal character is found
            # ValueError exception will be called.
            pattern = re.compile("[@_!#$%^&*()<>?/|}{~:]")
            if(pattern.search(sentence) is not None):
                raise ValueError

        except ValueError:
            # The cycle will go on until validation
            print("\033[1;31;40m Error! Please only use acceptable characters \n")

        # when a valid sentance is received
        # the loop will end.
        s_list = sentence.split()
        new_s = ""
        for s in s_list:
            words = get_words(s[0], len(s))
            # remove the word as we want a new word
            if s in words:
                words.remove(s)
            if words:
                new_s = f'{new_s} {random.choice(words)} '
            else:
                new_s = f'{new_s} {s}* '

        print("\033[1;32;40m Here is your Original sentance")
        print(f"\033[1;36;40m {sentence}\n")
        print("\033[1;32;40m Here is your new sentance!")
        print(f"\033[1;35;40m {new_s.lstrip()}\n")
        if '*' in new_s:
            print("\033[1;31;40m * denotes that there were no other words it could match, or it contains illegal characters.\n")
        time.sleep(1)
        print("\033[1;32;40m want to Try again? just enter a new sentance below!\n")


# The function is called
scrabble()
