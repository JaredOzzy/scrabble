# Scrabble

## Introduction

Welcome to the Scrabble Assistant!

To run the program you need the following installed:

- Python 3
- git

### Python Installation
- Download your respective python package from https://www.python.org/downloads/
- Install using the downloaded package and follow instructions

### git installation 
- Download your respective git package from https://git-scm.com/downloads
- Install using the downloaded package and follow instructions


## Setup
Once you have installed both python3 and git you can clone this repo using the following steps:
- open a your terminal or command line
- paste the following `git@gitlab.com:JaredOzzy/scrabble.git scrabble` and hit enter
- once repo is cloned go into the folder using `cd scrabble`

Once you are inside the repositories root folder you are able to run the program with the following command:
- python3 scrabble.py

## How to use it?
Once you run the program you will be prompted to enter a word/s as many as you like!
If you enter a illegal character you will be prompted to try again.

Once a successful sentance has been entered the program will replace each word a similar word,
the word you receive in return will begin with the same letter and be the same length as your previous word.

If we can't find a word from our list of words we will return the same word you orginally entered with an asteriks (*)

